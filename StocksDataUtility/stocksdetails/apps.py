from django.apps import AppConfig


class StocksdetailsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'stocksdetails'
