import json
from django.http import HttpResponse, JsonResponse
import redis
from django.conf import settings
import csv

redis_instance = redis.StrictRedis(host=settings.REDIS_HOST,
                                   port=settings.REDIS_PORT, db=0)

def readStockData(request):

    try:

        if request.method == 'GET':

            stockName = request.GET['name'].strip().upper()
            stockDetails = redis_instance.get(stockName)
            if(stockDetails != None):

                responseData = json.loads(stockDetails.decode('utf-8'))
                return JsonResponse({
                    "status": 200,
                    "message": "succes",
                    "result": responseData
                })

            else:
                return JsonResponse({
                    "status": 404,
                    "message": "not exist"
                })
    except:
        return JsonResponse({
            "status": 500,
            "message": "internal server error"
        })



def getAllStockNames(request):

    try:

        if request.method == 'GET':
            
            return JsonResponse({
                "status": 200,
                "message": "success",
                "result": [{
                    'text':x.decode('utf-8'),
                    'value':x.decode('utf-8')}
                    for x in redis_instance.keys()]
            })

        return JsonResponse({
            "status": 404,
            "message": "not exist"
        })
    except:
        return JsonResponse({
            "status": 500,
            "message": "internal server error"
        })




def generateCsv(request):

    try:
        if request.method == 'GET':
            response = HttpResponse(content_type='text/csv')
            header = json.loads(request.GET['columnNames'])
            writer = csv.DictWriter(response, fieldnames=header)
            writer.writeheader()

            rowHolder = {}
            
            for row in json.loads(request.GET['rowsData']):
                for key in row:
                    rowHolder[key] = row[key]
                writer.writerow(rowHolder)
                rowHolder ={}
            
            response['Content-Disposition'] = 'attachment; filename="export.csv"'
            return response  

    except:
        return JsonResponse({
            "status": 500,
            "message": "internal server error"
        })