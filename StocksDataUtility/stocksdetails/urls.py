
from django.urls import path
from . import views;

urlpatterns = [
    path('', views.readStockData,name="stock data"),

    path('convertcsv',views.generateCsv,name="generate csv"),
    path('stockslist',views.getAllStockNames,name="stocks list"),
]