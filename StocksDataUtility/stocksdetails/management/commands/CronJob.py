import logging

from django.conf import settings

from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.triggers.cron import CronTrigger
from django.core.management.base import BaseCommand
from django_apscheduler.jobstores import DjangoJobStore
from django_apscheduler.models import DjangoJobExecution
from .RedisManager import storeDataInRedis

logger = logging.getLogger(__name__)

def periodicJob():
    print("im working..>>>>")
    storeDataInRedis()
    return

def delete_old_job_executions(max_age=604_800):
    """This job deletes all apscheduler job executions older than `max_age` from the database."""
    DjangoJobExecution.objects.delete_old_job_executions(max_age)


class Command(BaseCommand):
    #help = "Runs apscheduler."
    def handle(self, *args, **options):

        storeDataInRedis()
        
        scheduler = BlockingScheduler(timezone=settings.TIME_ZONE)
        scheduler.add_jobstore(DjangoJobStore(), "default")

        scheduler.add_job(
            periodicJob,
            trigger=CronTrigger(day_of_week='mon-sun', hour=18, minute=00),
            id="periodic_Job",
            max_instances=1,
            replace_existing=True,
        )
        logger.info("Added job 'periodic Job'.")

        try:
            logger.info("Starting scheduler...")
            scheduler.start()
        except KeyboardInterrupt:
            logger.info("Stopping scheduler...")
            scheduler.shutdown()
            logger.info("Scheduler shut down successfully!")