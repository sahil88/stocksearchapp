import redis
import pandas as pd
import os
from django.conf import settings
import json
import requests
from datetime import datetime, timedelta

redis_instance = redis.StrictRedis(host=settings.REDIS_HOST,
                                   port=settings.REDIS_PORT, db=0)

lastDataUpdatedAt = ""

def downloadCsv():

    daysBefore = -1

    while True:
        daysBefore = daysBefore+1
        today = datetime.now()
        previousDay = datetime.now() - timedelta(days=daysBefore)
        fileDate = previousDay.strftime('%d%m%y')
        
        try:

            url = f'https://www.bseindia.com/download/BhavCopy/Equity/EQ{fileDate}_CSV.ZIP'
            response = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})

            if(response.ok):

                global lastDataUpdatedAt
                lastDataUpdatedAt = previousDay.strftime("%Y-%m-%d")
                print(lastDataUpdatedAt)
                fname = fileDate+'.zip'
                with open(fname, 'wb') as outfile:
                    outfile.write(response.content)
                return fname

        except:
            print("fetching file error")


def storeDataInRedis():

    csvFile = downloadCsv()
    df = pd.read_csv(csvFile)
    redis_instance.flushall()
    stockValue = {}

    for index, row in df.iterrows():

        stockValue.update(SC_CODE=row['SC_CODE'])
        stockValue.update(LOW=row['LOW'])
        stockValue.update(OPEN=row['OPEN'])
        stockValue.update(HIGH=row['HIGH'])
        stockValue.update(CLOSE=row['CLOSE'])
        stockValue.update(UPDATEDON=lastDataUpdatedAt)

        redis_instance.set(row['SC_NAME'].rstrip().upper(),
                        json.dumps(stockValue).encode('utf-8'))
    
    os.remove(csvFile)